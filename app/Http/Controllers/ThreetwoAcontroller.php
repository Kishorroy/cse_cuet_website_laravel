<?php

namespace App\Http\Controllers;

use App\ThreetwoA;
use Illuminate\Http\Request;

class ThreetwoAcontroller extends Controller
{
    public function store()
    {
        $objThreetwoA = new ThreetwoA();
        $objThreetwoA->sub_title = $_POST['sub_title'];
        $objThreetwoA->teacher_name = $_POST['teacher_name'];
        $objThreetwoA->day = $_POST['day'];
        $objThreetwoA->time = $_POST['time'];
        $status = $objThreetwoA->save();
        return redirect()->route('ThreetwoAroutine');
    }

    public function index()
    {
        $objThreetwoA = new ThreetwoA();
        $allData = $objThreetwoA->paginate(20);
        return view("ThreetwoA/index", compact('allData'));
    }

    public function index1()
    {
        $objThreetwoA = new ThreetwoA();
        $allData = $objThreetwoA->paginate(20);
        return view("ThreetwoA/index1", compact('allData'));
    }

    public function view4Edit($id)
    {
        $objThreetwoA = new ThreetwoA();
        $oneData = $objThreetwoA->find($id);
        return view("ThreetwoA/edit", compact('oneData'));
    }

    public function update()
    {
        $objThreetwoA = new ThreetwoA();
        $oneData = $objThreetwoA->find($_POST['id']);
        $oneData->sub_title = $_POST['sub_title'];
        $oneData->teacher_name = $_POST['teacher_name'];
        $oneData->day = $_POST['day'];
        $oneData->time = $_POST['time'];
        $status = $oneData->update();

        return redirect()->route('ThreetwoAroutine');
    }

    public function delete($id)
    {
        $objThreetwoA = new ThreetwoA();
        $oneData = $objThreetwoA->find($id)->delete();
        return redirect()->route('ThreetwoAroutine');
    }
}