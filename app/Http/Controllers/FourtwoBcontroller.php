<?php

namespace App\Http\Controllers;

use App\FourtwoB;
use Illuminate\Http\Request;

class FourtwoBcontroller extends Controller
{
    public function store(){
        $objFourtwoB=new FourtwoB();
        $objFourtwoB->sub_title=$_POST['sub_title'];
        $objFourtwoB->teacher_name=$_POST['teacher_name'];
        $objFourtwoB->day=$_POST['day'];
        $objFourtwoB->time=$_POST['time'];
        $status=$objFourtwoB->save();
        return redirect()->route('FourtwoBroutine');
    }
    public function index(){
        $objFourtwoB=new FourtwoB();
        $allData=$objFourtwoB->paginate(20);
        return view("FourtwoB/index",compact('allData'));
    }
    public function index1(){
        $objFourtwoB=new FourtwoB();
        $allData=$objFourtwoB->paginate(20);
        return view("FourtwoB/index1",compact('allData'));
    }

    public function view4Edit($id){
        $objFourtwoB=new FourtwoB();
        $oneData=$objFourtwoB->find($id);
        return view("FourtwoB/edit",compact('oneData'));
    }

    public function update(){
        $objFourtwoB=new FourtwoB();
        $oneData=$objFourtwoB->find($_POST['id']);
        $oneData->sub_title=$_POST['sub_title'];
        $oneData->teacher_name=$_POST['teacher_name'];
        $oneData->day=$_POST['day'];
        $oneData->time=$_POST['time'];
        $status=$oneData->update();

        return redirect()->route('FourtwoBroutine');
    }

    public function delete($id){
        $objFourtwoB=new FourtwoB();
        $oneData=$objFourtwoB->find($id)->delete();
        return redirect()->route('FourtwoBroutine');
    }

}
