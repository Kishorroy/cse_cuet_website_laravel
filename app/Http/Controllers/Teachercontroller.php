<?php

namespace App\Http\Controllers;

use App\Teacher;
use Illuminate\Http\Request;

class Teachercontroller extends Controller
{
    public function store(){
        $objAdmin=new Teacher();
        $objAdmin->t_name=$_POST['t_name'];
        $objAdmin->t_email=$_POST['t_email'];
        $objAdmin->t_designation=$_POST['t_designation'];
        $objAdmin->b_name=$_POST['b_name'];
        $objAdmin->contact=$_POST['contact'];
        $objAdmin->r_interest=$_POST['r_interest'];
        $objAdmin->b_sub=$_POST['b_sub'];
        $objAdmin->b_uni=$_POST['b_uni'];
        $objAdmin->b_country=$_POST['b_country'];
        $objAdmin->m_sub=$_POST['m_sub'];
        $objAdmin->m_uni=$_POST['m_uni'];
        $objAdmin->m_country=$_POST['m_country'];
        $objAdmin->p_sub=$_POST['p_sub'];
        $objAdmin->p_uni=$_POST['p_uni'];
        $status=$objAdmin->save();
        return view("Teacher/insert");
    }

//    public function index(){
//        $objAdmin=new Teacher();
//        $allData=$objAdmin->paginate(20);
//        return view("Teacher/index",compact('allData'));
//    }
//    public function index1(){
//        $objAdmin=new Teacher();
//        $allData=$objAdmin->paginate(20);
//        return view("Teacher/index1",compact('allData'));
//    }
//
//    public function view4Edit($id){
//        $objAdmin=new Teacher();
//        $oneData=$objAdmin->find($id);
//        return view("Teacher/edit",compact('oneData'));
//    }
//
//    public function update(){
//        $objAdmin=new Teacher();
//        $oneData=$objAdmin->find($_POST['id']);
//        $oneData->sub_title=$_POST['sub_title'];
//        $oneData->teacher_name=$_POST['teacher_name'];
//        $oneData->day=$_POST['day'];
//        $oneData->time=$_POST['time'];
//        $status=$oneData->update();
//
//        return redirect()->route('Adminroutine');
//    }
//
//    public function delete($id){
//        $objAdmin=new Teacher();
//        $oneData=$objAdmin->find($id)->delete();
//        return redirect()->route('Adminroutine');
//    }

}
