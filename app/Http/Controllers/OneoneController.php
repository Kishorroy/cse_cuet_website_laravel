<?php

namespace App\Http\Controllers;

use App\Oneone;
use Illuminate\Http\Request;

class OneoneController extends Controller
{
    public function store(){
        $objOneone=new Oneone();
        $objOneone->sub_title=$_POST['sub_title'];
        $objOneone->teacher_name=$_POST['teacher_name'];
        $objOneone->day=$_POST['day'];
        $objOneone->time=$_POST['time'];
        $status=$objOneone->save();
        return redirect()->route('Oneoneroutine');
    }
    public function index(){
        $objOneone=new Oneone();
        $allData=$objOneone->paginate(20);
        return view("Oneone/index",compact('allData'));
    }
    public function index1(){
        $objOneone=new Oneone();
        $allData=$objOneone->paginate(20);
        return view("Oneone/index1",compact('allData'));
    }
    public function view4Edit($id){
        $objOneone=new Oneone();
        $oneData=$objOneone->find($id);
        return view("Oneone/edit",compact('oneData'));
    }

    public function update(){
        $objOneone=new Oneone();
        $oneData=$objOneone->find($_POST['id']);
        $oneData->sub_title=$_POST['sub_title'];
        $oneData->teacher_name=$_POST['teacher_name'];
        $oneData->day=$_POST['day'];
        $oneData->time=$_POST['time'];
        $status=$oneData->update();

        return redirect()->route('Oneoneroutine');
    }
    public function delete($id){
        $objOneone=new Oneone();
        $oneData=$objOneone->find($id)->delete();
        return redirect()->route('Oneoneroutine');
    }

}
