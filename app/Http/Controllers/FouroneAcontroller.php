<?php

namespace App\Http\Controllers;

use App\FouroneA;
use Illuminate\Http\Request;

class FouroneAcontroller extends Controller
{
    public function store(){
        $objFouroneA=new FouroneA();
        $objFouroneA->sub_title=$_POST['sub_title'];
        $objFouroneA->teacher_name=$_POST['teacher_name'];
        $objFouroneA->day=$_POST['day'];
        $objFouroneA->time=$_POST['time'];
        $status=$objFouroneA->save();
        return redirect()->route('FouroneAroutine');
    }
    public function index(){
        $objFouroneA=new FouroneA();
        $allData=$objFouroneA->paginate(20);
        return view("FouroneA/index",compact('allData'));
    }
    public function index1(){
        $objFouroneA=new FouroneA();
        $allData=$objFouroneA->paginate(20);
        return view("FouroneA/index1",compact('allData'));
    }

    public function view4Edit($id){
        $objFouroneA=new FouroneA();
        $oneData=$objFouroneA->find($id);
        return view("FouroneA/edit",compact('oneData'));
    }

    public function update(){
        $objFouroneA=new FouroneA();
        $oneData=$objFouroneA->find($_POST['id']);
        $oneData->sub_title=$_POST['sub_title'];
        $oneData->teacher_name=$_POST['teacher_name'];
        $oneData->day=$_POST['day'];
        $oneData->time=$_POST['time'];
        $status=$oneData->update();

        return redirect()->route('FouroneAroutine');
    }

    public function delete($id){
        $objFouroneA=new FouroneA();
        $oneData=$objFouroneA->find($id)->delete();
        return redirect()->route('FouroneAroutine');
    }

}
