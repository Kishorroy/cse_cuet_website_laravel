<?php

namespace App\Http\Controllers;

use App\Research;
use Illuminate\Http\Request;

class ResearchviewController extends Controller
{
    public  function index(){
        $objResearch=new Research();
        $allData=$objResearch->paginate(5);
        return view('resarchView',compact('allData'));
    }
}
