<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>CUET CSE</title>

    <link rel="shortcut icon" href="{{URL::asset('/images/logo/cuet.png')}}" />
    <link rel="stylesheet" href="{{URL::asset('/Resources/bootstrap-3.3.7-dist/css/bootstrap.min.css')}}">

    <script src="{{URL::asset('/Resources/bootstrap-3.3.7-dist/js/jquery-3.2.0.min.js')}}"></script>
    <script src="{{URL::asset('/Resources/bootstrap-3.3.7-dist/js/bootstrap.min.js')}}"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

    <style>
        .container{
            width: 1350px;
            background: linear-gradient(white , #afd9ee, #afd9ee);
        }

        .header{
            height: 200px;
            background: white; /* For browsers that do not support gradients */
            background: -webkit-linear-gradient(white , white, #afd9ee); /* For Safari 5.1 to 6.0 */
            background: -o-linear-gradient(white , white, #afd9ee); /* For Opera 11.1 to 12.0 */
            background: -moz-linear-gradient(white , white, #afd9ee); /* For Firefox 3.6 to 15 */
            background: linear-gradient(white , white, #afd9ee); /* Standard syntax (must be last) */
        }
        .navbar-default{
            background: linear-gradient(white ,#afd9ee , #afd9ee);
        }

        .middle{
            background: white; /* For browsers that do not support gradients */
            background: -webkit-linear-gradient(#afd9ee,white , white, #afd9ee); /* For Safari 5.1 to 6.0 */
            background: -o-linear-gradient(#afd9ee,white , white, #afd9ee); /* For Opera 11.1 to 12.0 */
            background: -moz-linear-gradient(#afd9ee,white , white, #afd9ee); /* For Firefox 3.6 to 15 */
            background: linear-gradient(#afd9ee ,white , white, #afd9ee); /* Standard syntax (must be last) */
        }
    </style>

</head>
<body>

<div class="container">

    <div class="header">
        <center><img src="{{URL::asset('/images/banner/Untitled.jpg')}}"></img></center>
        <br><br>
        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <div class="navbar-header" >
                    <a class="navbar-brand" href="#" style="color: black">CSE,CUET</a>
                </div>
                <ul class="nav navbar-nav">
                    <li><a href="home1.blade.php">Home</a></li>
                    <li><a href="admission.blade.php">Course Info</a></li>
                    <li class="active"><a href="/faculty" >Faculty Members</a></li>
                    <li><a href="notice.blade.php">Notice Board</a></li>

                    <li><a href="class_routine.blade.php">Class Routine</a></li>
                    <li><a href="research.blade.php">Research</a></li>

                    <li><a href="contact.blade.php">Contact Info</a></li>
                </ul>

            </div>
        </nav>

    </div>
		
    <div class="container">
        <div class="col-sm-12">
            <h2 align="center"><b>FACULTY MEMBERS</b><hr></h2>
        </div>
        <div class="col-sm-12">
            <h2 align="center">HEAD OF THE DEPARTMENT<hr></h2>
        </div>
         @foreach($retrivedData['head'] as $value=>$datas)
        <div class="col-md-12">
            <div class="col-md-4"></div>
            <div class="col-md-4" style=" text-align: center;">
		<img src="data:image/jpeg;base64,{!!$datas['image']!!}" style="border-radius: 100%">
                <div class="text"style="padding-top: 10px">
                     <p style="text-align: center">{!!$datas['name']!!}</p>
                     <p style="text-align: center"><b>Designation:</b>{!!$datas['Designation']!!}</p>

                    <div class="col-sm-4"></div>
                    <div class="button col-sm-4" style="width:134px; height: 30px;background-color: #46b8da;text-align: center;padding-top: 5px;margin-bottom: 10px">
                        <a href="faculty/{!!$datas['id']!!}" style="color: white;">Read Full Profile</a>
                    </div>
                    <div class="col-sm-4"></div>

                 </div>
            </div>
            <div class="col-md-4"></div>
        </div>
        @endforeach

        <div class="col-sm-12">
            <h2 align="center">PROFESSORS<hr></h2>
        </div>
			@foreach($retrivedData['Professor'] as $value=>$datas)

            <div class="col-md-3" style=" text-align: center;">
                <img src="data:image/jpeg;base64,{!!$datas['image']!!}" style="border-radius: 100%">
                <div class="text"style="padding-top: 10px">
                    <p style="text-align: center">{!!$datas['name']!!}</p>
                    <p style="text-align: center"><b>Designation:</b>{!!$datas['Designation']!!}</p>
                    <div class="button col-sm-3" style="width:134px; height: 30px;background-color: #46b8da;text-align: center;padding-top: 5px; margin-bottom: 10px; margin-left: 90px">
                        <a href="faculty/{!!$datas['id']!!}" style="color: white;">Read Full</a>
                    </div>
                 </div>
            </div>

        @endforeach

        <div class="col-sm-12">
            <h2 align="center">ASSOCIATE PROFESSORS<hr></h2>
        </div>

        <div class="col-md-12" style="margin-top: 20px">
						@foreach($retrivedData['AsscProfessor'] as $value=>$datas)


            <div class="col-md-3" style=" text-align: center; ;">
                <img src="data:image/jpeg;base64,{!!$datas['image']!!}" alt="Profile Picture" style="border-radius: 100%">
                <div class="text"style="padding-top: 10px">
                    <p style="text-align: center">{!!$datas['name']!!}</p>
                    <p style="text-align: center"><b>Designation:</b>{!!$datas['Designation']!!}</p>
                    <div class="button col-sm-3" style="width:134px; height: 30px;background-color: #46b8da;text-align: center;padding-top: 5px;margin-bottom: 10px; margin-left: 75px">
                        <a href="faculty/{!!$datas['id']!!}" style="color: white;">Read Full</a>
                    </div>
                 </div>
            </div>

        @endforeach
        </div>

        <div class="col-sm-12">
            <h2 align="center">ASSISTANT PROFESSORS<hr></h2>
        </div>
				<div class="col-md-12" style="margin-top: 20px">
						@foreach($retrivedData['AsstProfessor'] as $value=>$datas)


            <div class="col-md-3" style=" text-align: center; ;">
                <img src="data:image/jpeg;base64,{!!$datas['image']!!}" alt="Profile Picture" style="border-radius: 100%">
                <div class="text"style="padding-top: 10px">
                    <p style="text-align: center">{!!$datas['name']!!}</p>
                    <p style="text-align: center"><b>Designation:</b>{!!$datas['Designation']!!}</p>
                    <div class="button col-sm-3" style="width:134px; height: 30px;background-color: #46b8da;text-align: center;padding-top: 5px;margin-bottom: 10px; margin-left: 75px">
                        <a href="faculty/{!!$datas['id']!!}" style="color: white;">Read Full</a>
                    </div>
                 </div>
            </div>

        @endforeach
        </div>
         <div class="col-sm-12">
            <h2 align="center">LECTURERS<hr></h2>
        </div>

       <div class="col-md-12" style="margin-top: 20px">
						@foreach($retrivedData['Lecturer'] as $value=>$datas)


            <div class="col-md-3" style=" text-align: center">
                <img src="data:image/jpeg;base64,{!!$datas['image']!!}" alt="Profile Picture" style="border-radius: 100%">
                <div class="text"style="padding-top: 10px">
                    <p style="text-align: center">{!!$datas['name']!!}</p>
                    <p style="text-align: center"><b>Designation:</b>{!!$datas['Designation']!!}</p>
                    <div class="button col-sm-3" style="width:134px; height: 30px;background-color: #46b8da;text-align: center;padding-top: 5px;margin-bottom: 10px; margin-left: 75px">
                        <a href="faculty/{!!$datas['id']!!}" style="color: white;">Read Full</a>
                    </div>
                 </div>
            </div>
        @endforeach
        </div>


</div>


    </div>


</div>
<div class="footer" style="height:50px; background: linear-gradient(white ,#afd9ee , #afd9ee); ">
    <br>
    <center><b style="color: white"> Copyright &#169; Department of CSE, CUET| 2017</b> </center>

</div>

</body>
</html>
