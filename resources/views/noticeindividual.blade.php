@extends('custom')

@section('content')

<div class="container">

    <h3> Notice Board </h3>
    <hr>

    <ol>
        @foreach($noticeData as $key=>$value)
        <img src="/images/notice/{!! $value->notice_path !!}" alt="notice" style="padding: 10%;width: 1300px">
        @endforeach
    </ol>

    @endsection

</div>