@extends('../layouts/app')

<head>

    <style>

        td{

            width:100px;
            height:50px


        }

        .container{
            position: absolute;
            left: 50px;

        }


    </style>

</head>


@section('content')

<div class="container">


        <a href="create"><button type="button" class="btn btn-primary btn-lg">Add New</button></a>

    <br> <br>


    <b style="color: red">  Total: {!! $allData->total() !!} Record(s) <br>

    Showing: {!! $allData->count() !!} Record(s) <br>

    {!! $allData->links() !!}

    </b>



    <table class="table table-bordered table table-striped" >

        <th>Author Name</th>
        <th>Paper Name</th>
        <th>Type</th>
        <th>Journal/Conference Name</th>
        <th>Publication year</th>

        <th>Action Buttons</th>

        @foreach($allData as $oneData)

            <tr>

                <td>  {!! $oneData['author_name'] !!} </td>
                <td>  {!! $oneData['paper_name'] !!} </td>
                <td>  {!! $oneData['type'] !!} </td>
                <td>  {!! $oneData['journal_name'] !!} </td>
                <td>  {!! $oneData['publication_year'] !!} </td>


                <td>
                    <a href="edit/{!! $oneData['id'] !!}"><button class="btn btn-primary">Edit</button></a>
                    <a href="delete/{!! $oneData['id'] !!}"><button class="btn btn-danger">Delete</button></a>

                </td>

            </tr>


        @endforeach


    </table>
    {!! $allData->links() !!}
</div>

    @endsection
